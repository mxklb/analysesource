# Description
AnalyseSources searches through text-files inside a given folder and analyses
the amount of lines in each file and prints them to std::out. It's possible to
filter out files with different file endings. Additional some low level statistic
such as mean/stddev/min/max/median for the complete amount of files inside the
folder is calculated.

## Dependencies
This program uses Qt, see: [qt-project.org](http://qt-project.org/)

CI build state: [![Run Status](https://api.shippable.com/projects/58aec0a432ce931100173ab7/badge?branch=master)](https://app.shippable.com/projects/58aec0a432ce931100173ab7)
[![build status](https://gitlab.com/mxklb/analysesource/badges/master/build.svg)](https://gitlab.com/mxklb/analysesource/commits/master)

## Build steps

    qmake
    make

## Usage
    AnalyseSources -p "/Absolute/Path/To/Folder" -e "Ending1,Ending2,..." -s -h

Per default if no arguments are given or missing, the following parameters are used:

    -p := The actual path where the executable is located
    -e := Searches for three types of files (endings *.c,*.cpp,*.h)
    -r := Searches recursive through all folders

The parameter -s changes the sorting of the std:out. If this parameter is missing
the files presented at std:out will be sorted according to their files-names, else:

    -s := Files std::out will be sorted according to each files amount of lines.
    -h := Shows help text.

## Examples
    AnalyseSources -p "C:\my sourcefolder" -e "*.hpp,*.txt" -s
    AnalyseSources -e *.hpp,*.txt -p C:\sourcefolder
    AnalyseSources -s

Note: There must be no spaces inside the path or endings, if so use quotes!
