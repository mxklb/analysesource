/*
 * Copyright (C) 2013 by xamblak <dev@xamblak.de>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 * and associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial
 * portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#include <QDir>
#include <QTextStream>
#include <iostream>
#include <QtPlugin>
#include <QMap>
#include <QMapIterator>
#include <QDirIterator>

#include <math.h>
#include <limits.h>

#include "parsearguments.h"

using namespace std;


/*
 * Lists all files with given ending recrusively (subdirectories)
 */
QStringList scanDirIter(QDir dir, QString ending)
{
    QStringList fileList;
    QDirIterator iterator(dir.absolutePath(), QDirIterator::Subdirectories);
    while (iterator.hasNext())
    {
        iterator.next();
        if (!iterator.fileInfo().isDir() && !iterator.fileInfo().isSymLink())
        {
            QString filename = iterator.fileName();
            if ( filename.endsWith(ending) )
            {
               // QFileInfo fi(filename);
               // if( fi.symLinkTarget().isEmpty() )
                {
                    fileList.append(iterator.filePath());
                }
            }
        }
    }
    return fileList;
}



int main(int argc, char *argv[])
{
    //QCoreApplication a(argc, argv);

    ParseArguments parser(argc, argv);
    if( parser.isHelpShown )
        return 0;

    bool sortByLength = parser.sort;
    bool recrusive = parser.recrusive;
    QStringList nameFilters = parser.nameFilters;
    QString path = parser.path;
    QDir myDir(QDir::toNativeSeparators(path));


    long fileCount = 0;
    QStringList fileList;
    QList<QStringList> fileTypeList;

    if( recrusive )
    {
        foreach(QString ending, nameFilters)
        {
            fileList = scanDirIter(myDir, ending.mid(1));
            fileTypeList.append(fileList);
            fileCount += fileList.size();
        }
    }
    else
    {
        for( int i=0; i<nameFilters.size(); i++ ){
            QStringList fileTypeFilter(nameFilters.at(i));
            myDir.setNameFilters(fileTypeFilter);
            fileList = myDir.entryList(fileTypeFilter, QDir::Files | QDir::NoSymLinks, QDir::Name | QDir::IgnoreCase);
            fileTypeList.append(fileList);
            fileCount += fileList.size();
        }
    }

    cout << "Sum of files found = " << fileCount << endl;


    for( int j=0; j<fileTypeList.size(); j++ ){

        long sumOfLines = 0;
        long minLineCount = LONG_MAX;
        long maxLineCount = 0;
        //QList<long> filesLineCount;
        QMap<QString, long> filesLineCount;
        fileList = fileTypeList.at(j);
        cout << "\n---------------------------------------------" << endl;
        cout << "Amount of " << nameFilters.at(j).toStdString() << " files = " << fileList.size() << endl;
        cout << "---------------------------------------------" << endl;

        for( int i=0; i<fileList.size(); i++ ){

            if( !path.isEmpty() )
                path += "\\";

            long lineCount = 0;
            QString absoluteFilePath = QDir::toNativeSeparators(path + fileList.at(i));
            QFile file(absoluteFilePath);
            QFileInfo fileInfo(absoluteFilePath);

            if( fileInfo.exists() ){
                if (!file.open(QIODevice::ReadOnly | QIODevice::Text)){
                    cout << "Failed to open file: " << fileInfo.filePath().toStdString() << endl;
                    continue;
                }

                QTextStream in(&file);
                QString line = in.readLine();
                while (!line.isNull()) {
                    lineCount++;
                    line = in.readLine();
                }

                if( !sortByLength )
                    cout << "Count of lines = " << lineCount << " \tfile: " << fileInfo.filePath().toStdString() << endl;

                filesLineCount.insert(fileInfo.filePath(), lineCount);
                sumOfLines += lineCount;

                // Remember min/max lineCount
                if( lineCount > maxLineCount )
                    maxLineCount = lineCount;
                if( lineCount < minLineCount )
                    minLineCount = lineCount;
            }
            else{
                cout << "File not found: " << fileInfo.filePath().toStdString() << endl;
                continue;
            }
        }

        if( filesLineCount.size() == 0 ){
            cout << "Error: No files found!" << endl;
            continue;
        }

        // Calculate standard deviation
        double accumulator = 0.;
        double standardDeviation = 0.;
        double meanLength = (double)sumOfLines/filesLineCount.size();
        QMap<long, QString> sortedLengthMap;
        QMapIterator<QString, long> i(filesLineCount);
        while( i.hasNext() ){
            i.next();
            double tmp = i.value() - meanLength;
            accumulator += tmp*tmp;
            sortedLengthMap.insertMulti(i.value(),i.key());
        }
        if( filesLineCount.size() > 1 )
            standardDeviation = sqrtf(accumulator/(filesLineCount.size()-1));
        else standardDeviation = 0;

        // Calculate median
        QList<long> sortedLines = filesLineCount.values();
        qSort(sortedLines.begin(), sortedLines.end());
        double median = 0;
        if( sortedLines.size() % 2 == 1 )
            median = sortedLines.at((sortedLines.size()-1)/2);
        else {
            median = sortedLines.at((sortedLines.size()-1)/2);
            median += sortedLines.at((sortedLines.size()-1)/2+1);
            median /= 2.;
        }

        if( sortByLength ){
            QMapIterator<long, QString> itr(sortedLengthMap);
            while( itr.hasNext() ){
                itr.next();
                long length = itr.key();
                QString fileName = itr.value();
                cout << "Count of lines = " << length << " \tfile: " << fileName.toStdString() << endl;
            }
        }

        cout << "---------------------------------------------" << endl;
        cout << "Mean line count \t= " << (long)(meanLength+0.5) << endl;
        cout << "Standard deviation \t= " << (long)(standardDeviation+0.5) << endl;
        cout << "Maximum line count \t= " << maxLineCount << endl;
        cout << "Median line count \t= " << (long)(median+0.5) << endl;
        cout << "Minimum line count \t= " << minLineCount << endl;
        cout << "---------------------------------------------" << endl;
    }

    return 0;;
}
