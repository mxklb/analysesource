/*
 * Copyright (C) 2013 by xamblak <dev@xamblak.de>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 * and associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial
 * portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#include "parsearguments.h"

#include <iostream>

using namespace std;

ParseArguments::ParseArguments(int argc, char *argv[])
{
    path = "";
    sort = false;
    recrusive = false;
    isHelpShown = false;
    nameFilters.append("*.cpp");
    nameFilters.append("*.c");
    nameFilters.append("*.h");

    if( argc == 1 )
        return;

    QStringList strList;
    for (int n = 1; n < argc; n++){
        strList.append(argv[n]);
        //cout << "pos " << n << " arg: " << strList.at(n-1).toStdString() << endl;
    }

    // Search path (-p)
    int pathPos = strList.indexOf(QRegExp("-p")) + 1;
    if( pathPos > 0 ){
        if( pathPos < strList.size() ){
            path = strList.at(pathPos);
        }
        else{
            cout << "Warning: Missing definition -p, the default path is used!" << endl;
        }
    }

    // Search file endings (-e)
    int endingPos = strList.indexOf(QRegExp("-e")) + 1;
    if( endingPos > 0 ){
        if( endingPos < strList.size() )
        {
             QString endings = strList.at(endingPos);
             nameFilters = endings.split(",", QString::SkipEmptyParts);
        }
        else{
            cout << "Warning: Missing definition -e, the default endings are used!" << endl;
        }
    }

    // Search for sorting option (-s)
    int sortPos = strList.indexOf(QRegExp("-s"));
    if( sortPos != -1 ){
        sort = true;
    }

    // Search for recrusive option (-r)
    sortPos = strList.indexOf(QRegExp("-r"));
    if( sortPos != -1 ){
        recrusive = true;
    }

    // Search for help option (-h)
    int helpPos = strList.indexOf(QRegExp("-h"));
    if( helpPos != -1 ){
        displayHelpText();
    }
    // Search for help option (-help)
    helpPos = strList.indexOf(QRegExp("-help"));
    if( helpPos != -1 ){
        displayHelpText();
    }
}

void ParseArguments::displayHelpText(){
    isHelpShown = true;
    cout << "AnalyseSources searches through text-files inside a given folder and analyse" << endl;
    cout << "the amount of lines in each file and prints it to std::out. It's possible to" << endl;
    cout << "filter out files with differnt fileendings. Additional some low level statistic" << endl;
    cout << "such as mean/stddev/min/max/median for the complete amount of files inside the" << endl;
    cout << "folder is calculated.\n" << endl;

    cout << "Dependencies:" << endl;
    cout << "This programm uses Qt, see: http://qt.nokia.com/products/" << endl;
    cout << "Win: QtCore4.dll is dynamically linked and has to be inside the executable folder.\n" << endl;

    cout << "Usage:" << endl;
    cout << "AnalyseSources.exe -p AbsolutePathToFolder -e Ending1,Ending2,... -s -h\n" << endl;

    cout << "Per default if no arguments are given or missing, the following parameters are used:" << endl;
    cout << "-p := The actual path where the executable is located" << endl;
    cout << "-e := Searches for three types of files (endings *.c,*.cpp,*.h)" << endl;
    cout << "-r := Searches recrusive through all folders\n" << endl;

    cout << "The parameter -s changes the sorting of the std:out. If this parameter is missing" << endl;
    cout << "the files presented at std:out will be sorted according to their files-names, else: " << endl;
    cout << "-s := Files std::out will be sorted according to each files amount of lines." << endl;
    cout << "-r := Recrusive, include files in subfolders.\n" << endl;
    cout << "-h := Shows this help text.\n" << endl;

    cout << "Example:" << endl;
    cout << "AnalyseSources.exe -p C:/sourcefolder -e *.hpp,*.txt -s" << endl;
}
